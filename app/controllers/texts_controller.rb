class TextsController < ApplicationController
	def break_text(t)
		exc_words = ['в','при','к','с','а']

		# разделение на слова с исключением не нужных
		t = t.mb_chars.downcase.split(/ /) - exc_words
		#exc_words.map { |e| t.delete(e) }
		return t
	end

	def index
		
	end

	def create
		@text = params[:text_form][:text]
		@text_res = break_text(@text)
		render :index
	end
end
